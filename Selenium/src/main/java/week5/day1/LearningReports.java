package week5.day1;

import java.io.IOException;

import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class LearningReports {
	@Test
	public void report() throws IOException {
	ExtentHtmlReporter html = new ExtentHtmlReporter("./reports/result.html");
	html.setAppendExisting(true);
	ExtentReports extent = new ExtentReports();
	extent.attachReporter(html);
	ExtentTest logger = extent.createTest("TC001", "Confidential");
	logger.assignAuthor("Kamal");
	logger.assignCategory("Regression");
	logger.log(Status.PASS, "Logged in Successfull", MediaEntityBuilder.createScreenCaptureFromPath("./Snaps/img1.png").build());
	logger.log(Status.PASS, "Verification is Successfull", MediaEntityBuilder.createScreenCaptureFromPath("./Snaps/img2.png").build());
	logger.log(Status.PASS, "Logout in Successfull", MediaEntityBuilder.createScreenCaptureFromPath("./Snaps/img3.png").build());
	extent.flush();
	}
}
