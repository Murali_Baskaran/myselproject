package week4.day2;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;

public class FlipkartIphone {

	public static void main(String[] args) throws InterruptedException {
		// Flipkart
		System.setProperty("webdriver.chrome.driver","./driver/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("https://www.flipkart.com");
		driver.manage().window().maximize();
		Thread.sleep(2000);
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		Set<String> allwins = driver.getWindowHandles();
		List<String> wins= new ArrayList<>();
		wins.addAll(allwins);
		driver.switchTo().window(wins.get(1));
		driver.close();
		driver.switchTo().window(wins.get(0));
		driver.findElementByName("q").sendKeys("iphone x");
		driver.findElementByXPath("//button[@type='submit']");
		System.out.println(driver.findElementByXPath("(//div[contains(text(),'₹')])[5]").getText());
		System.out.println(driver.findElementByXPath("(//div[contains(text(),'₹')])[6]").getText());
				
	}

}
