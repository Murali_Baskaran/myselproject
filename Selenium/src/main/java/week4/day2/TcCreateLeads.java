package week4.day2;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

public class TcCreateLeads extends Wrappers.SeMethods{
	@Test
	public void createLeads() {
		startApp("chrome", "http://leaftaps.com/opentaps");
		WebElement eleUserName = locateElement("id", "username");
		type(eleUserName, "DemoSalesManager");
		WebElement elePassword = locateElement("password");
		type(elePassword, "crmsfa");
		WebElement eleLogin = locateElement("class","decorativeSubmit");
		click(eleLogin);
		WebElement crmsfaLink = locateElement("linktext","CRM/SFA");
		clickWithoutSnap(crmsfaLink);
		click(locateElement("linktext","Leads"));
		click(locateElement("linktext","Create Lead"));
		type(locateElement("createLeadForm_companyName"),"Wipro");
		//click(locateElement("xpath", "//a[contains(@href,'parent')]"));
		type(locateElement("createLeadForm_firstName"),"Kokki");
		type(locateElement("createLeadForm_lastName"),"Kumar");
		selectDropDownUsingText(locateElement("createLeadForm_dataSourceId"), "Employee");
		selectDropDownUsingIndex(locateElement("createLeadForm_marketingCampaignId"), 1);
		//click(locateElement("xpath", "//input[@value='Create Lead']"));
	}
	
}
