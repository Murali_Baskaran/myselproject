package week4.day1;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.chrome.ChromeDriver;

public class WindowHandle {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "./driver/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("https://www.irctc.co.in/eticketing/loginHome.jsf");
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		driver.findElementByLinkText("AGENT LOGIN").click();
		driver.findElementByLinkText("Contact Us").click();
		Set<String> allWin = driver.getWindowHandles();
		List<String> window = new ArrayList<>();
		window.addAll(allWin);
		driver.switchTo().window(window.get(1));
		String title = driver.getTitle();
		System.out.println(title);
		driver.switchTo().window(window.get(0));
		driver.close();
		driver.switchTo().window(window.get(1));
		File src = driver.getScreenshotAs(OutputType.FILE);
		File loc = new File("./Snaps/img1.jpeg");
		FileUtils.copyFile(src,loc);

	}

}
