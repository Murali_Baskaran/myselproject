package week1.day1;

public class MobilePhone {
	public void dialCaller(String callerName) {
		System.out.println("Calling "+callerName);
	}
	public void sendMessage(long number) {
		System.out.println("Sending message to:"+number);
	}
}
