package week2.day1;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class EmailIDRegEx {

	public static void main(String[] args) {
		//E-mail Id validation
		String text="test.leaf@gmail.com";
		String var1="[\\w\\W]{8,}[\\W{1}[a-zA-Z{2,10}[\\w\\W{4,}]]]]";
		Pattern var2 = Pattern.compile(var1);
		Matcher var3 = var2.matcher(text);
		System.out.println(var3.matches());
		
		//Take the numbers out from the given string
		//String text11="1a2s3d4f";
		String text1 = text.replaceAll("[a-zA-Z]", "");
		System.out.println("Numbers are:"+text1);
		}

}
