package week2.day2;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

public class ReverseList {

	public static void main(String[] args) {
		// Print the list of numbers in reverse
		reverseList();
		oddPos();
	}
	public static void reverseList() {
		List<Integer> revList = new ArrayList<>();
		revList.add(1);
		revList.add(2);
		revList.add(3);
		System.out.println(revList);
		List<Integer> revList1 = new ArrayList<>();
		for(int i=revList.size()-1;i>=0;i--){
			revList1.add(revList.get(i));
		}
		System.out.println(revList1);
	}
	//Display only the numbers in odd pos
	public static void oddPos() {
		Set<Integer> oddPos = new TreeSet<>();
		oddPos.add(1);
		oddPos.add(2);
		oddPos.add(3);
		oddPos.add(4);
		oddPos.add(5);
		System.out.println(oddPos);
		List<Integer> oddPos1 = new ArrayList<>();
		oddPos1.addAll(oddPos);
		for(int i=0;i<oddPos1.size();i++) {
			if(i%2==1) {
				System.out.println(oddPos1.get(i));
			}
		}
			
	}
}
