package week2.day2;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class InterfaceDuplicateCount {

	public static void main(String[] args) {
		// fetch the count of the duplicate values
		InterfaceDupCount();
	}
	public static void InterfaceDupCount() {
		List<String> arrList = new ArrayList<>();
		arrList.add("Megha");
		arrList.add("Veerasamy");
		arrList.add("Enthiran");
		arrList.add("Megha");
		arrList.add("Guru");
		arrList.add("Megha");
		int var1 = arrList.size();
		Set<String> arrMovies = new HashSet<>();
		arrMovies.addAll(arrList);
		int var2 = arrMovies.size();
		if(var1-var2>0){
			System.out.println("Duplicate count is:"+(var1-var2+1));
		}else {
			System.out.println("No duplicates");
		}
	}

}
