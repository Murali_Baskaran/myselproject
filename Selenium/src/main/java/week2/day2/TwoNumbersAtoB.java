package week2.day2;

import java.util.Scanner;

public class TwoNumbersAtoB {

	public static void main(String[] args) {
		// get 2 numbers and print
		Scanner getin = new Scanner(System.in);
		System.out.println("Enter starting number:");
		int start = getin.nextInt();
		System.out.println("Enter ending number:");
		int end = getin.nextInt();
		for(int i=start;i<=end;i++) {
			if(i%3==0&&i%5==0)
				System.out.print(" FIZZBUZZ ");
			else if(i%3==0)
				System.out.print(" FIZZ ");
			else if(i%5==0)
				System.out.print(" BUZZ ");
			else
				System.out.print(" "+i+" ");
		}
		getin.close();
	}

}
