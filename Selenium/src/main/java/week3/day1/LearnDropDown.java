package week3.day1;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class LearnDropDown {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "./driver/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("http://leaftaps.com/opentaps");
		driver.manage().window().maximize();
		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Create Lead").click();
		WebElement dd1 = driver.findElementById("createLeadForm_dataSourceId");
		Select dd11 = new Select(dd1);
		dd11.selectByVisibleText("Public Relations");
		WebElement dd2 = driver.findElementById("createLeadForm_marketingCampaignId");
		Select dd22 = new Select(dd2);
		dd22.selectByValue("DEMO_MKTG_CAMP");
		WebElement dd3 = driver.findElementById("createLeadForm_industryEnumId");
		Select dd33 = new Select(dd3);
		List<WebElement> ddM = dd33.getOptions();
		for(WebElement swM:ddM) {
			String temp = swM.getText();
			if(temp.startsWith("M")) {	
				System.out.println(swM.getText());
			}
		}
		
		
		
		
		
		

	}

}
