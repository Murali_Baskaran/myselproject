package week3.day1;

import org.openqa.selenium.chrome.ChromeDriver;

public class SeleniumDemo {
public static void main(String[] args) {
	System.setProperty("webdriver.chrome.driver", "./driver/chromedriver.exe");
	ChromeDriver driver = new ChromeDriver();
	//Invoke Browser
	driver.get("http://leaftaps.com/opentaps");
	driver.manage().window().maximize();
	//Enter credentials
	driver.findElementById("username").sendKeys("DemoSalesManager");
	driver.findElementById("password").sendKeys("crmsfa");
	driver.findElementByClassName("decorativeSubmit").click();
	//click on CRM/SA
	driver.findElementByLinkText("CRM/SFA").click();
	//Create Lead
	driver.findElementByLinkText("Create Lead").click();
	//Enter details
	driver.findElementById("createLeadForm_companyName").sendKeys("TestLeaves");
	driver.findElementById("createLeadForm_firstName").sendKeys("Test");
	driver.findElementById("createLeadForm_lastName").sendKeys("Leaf");
	driver.findElementByName("submitButton").click();
	System.out.println("New Lead has been created");
		
}
}