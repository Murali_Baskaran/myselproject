package Wrappers;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeMethod;
/*import org.openqa.selenium.WebElement;*/
import org.testng.annotations.Test;
public class DeleteLeads extends ProjectMethods{
	@BeforeMethod
	public void testCaseDetails() {
		testCaseName = "Delete Leads";
		testDesc = "Deleting Leads";
		category = "Smoke";
		author = "Kamal";
	}
	//@Test(timeOut=15000)
	//@Test(groups= {"sanity"},dependsOnGroups= {"smoke"})
	@Test(groups= {"sanity"})
	public void deleteLeads() {
		try {
			WebElement crmsfaLink = locateElement("linktext","CRM/SFA");
			clickWithoutSnap(crmsfaLink);
			click(locateElement("linktext","Leads"));
			//String leadID = getText(locateElement("xpath", "((//table[contains(@class,'row-table')])[1]//a)[1]"));
			click(locateElement("xpath", "((//table[contains(@class,'row-table')])[1]//a)[1]"));
			click(locateElement("linktext","Delete"));
			click(locateElement("name", "submitButton"));
		}catch (WebDriverException e) {
			reportStep("Fail", "Delete Leads failed");
		}
	}
	
}
