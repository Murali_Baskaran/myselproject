package Wrappers;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.Select;

import utils.Report;

public class SeMethods extends Report implements WdMethods{
	public int i = 1;
	public RemoteWebDriver driver;

	public void startApp(String browser, String url) {
		try {
			if (browser.equalsIgnoreCase("chrome")) {			
				System.setProperty("webdriver.chrome.driver", "./driver/chromedriver.exe");
				driver = new ChromeDriver();
			} else if (browser.equalsIgnoreCase("firefox")) {			
				System.setProperty("webdriver.gecko.driver", "./driver/geckodriver.exe");
				driver = new FirefoxDriver();
			}
			driver.get(url);
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			//System.out.println("The Browser "+browser+" Launched Successfully");
			reportStep("Pass", "The Browser "+browser+" is Launched Successfully");
		} catch (WebDriverException e) {
			//System.out.println("The Browser "+browser+" not Launched ");
			reportStep("Fail", "The Browser "+browser+" not Launched");
		} finally {
			takeSnap();			
		}
	}
	public WebElement locateElement(String locator, String locValue) {
		try {
			switch(locator) {
			case "id": return driver.findElementById(locValue);
			case "class": return driver.findElementByClassName(locValue);
			case "xpath": return driver.findElementByXPath(locValue);
			case "linktext": return driver.findElementByLinkText(locValue);
			case "name": return driver.findElementByName(locValue);
			}
			reportStep("Pass", "Element "+locValue+" is found");
		} catch (NoSuchElementException e) {
			//System.out.println("The Element Is Not Located ");
			reportStep("Fail", "The Element Is Not Located");
		}finally {
			takeSnap();			
		}
		return null;
	}

	@Override
	public WebElement locateElement(String locValue) {
		try {
			reportStep("Pass", "Element "+locValue+" is found");
			return driver.findElementById(locValue);
			} catch (NoSuchElementException e) {
				reportStep("Fail", "The Element Is Not Located");
			}finally {
				takeSnap();			
			}
		return null;
	}

	@Override
	public void type(WebElement ele, String data) {
		try{
			ele.sendKeys(data);
			reportStep("Pass", "The Data "+data+" is Entered Successfully");
		} catch (NoSuchElementException e) {
			reportStep("Fail", "The Element Is Not Located"); 
		}finally {
			takeSnap();			
		}
	}

	@Override
	public void click(WebElement ele) {
		try{
			ele.click();
			reportStep("Pass", "The Element "+ele+" Clicked Successfully");
		}catch(NoSuchElementException e) {
			reportStep("Fail", "The Element "+ele+" is not clicked"); 
		}finally {
			takeSnap();			
		}
	}
	
	public void clickWithoutSnap(WebElement ele) {
		try {
			ele.click();
			reportStep("Pass", "The Element "+ele+" Clicked Successfully");
			}catch(NoSuchElementException e) {
				reportStep("Fail", "The Element "+ele+" is not clicked"); 
			}finally {
				takeSnap();			
		}
	}

	@Override
	public String getText(WebElement ele) {
		try {
			String text = ele.getText();
			reportStep("Pass", "Text fetched is "+text);
			return text;
		}catch(NoSuchElementException e) {
			reportStep("Fail", "The Element "+ele+" is not clicked"); 
		}finally {
			takeSnap();			
		}
		return null;
	}

	@Override
	public void selectDropDownUsingText(WebElement ele, String value) {
		try{
			Select dd = new Select(ele);
			dd.selectByVisibleText(value);
			reportStep("Pass", "The DropDown Is Selected with "+value);
		}catch(NoSuchElementException e) {
			reportStep("Fail", "The Element "+ele+" is not clicked"); 
		}finally {
			takeSnap();			
		}
	}

	@Override
	public void selectDropDownUsingIndex(WebElement ele, int index) {
		try{
			Select dd = new Select(ele);
			dd.selectByIndex(index);
			reportStep("Pass", "The DropDown Is Selected with the option:"+index);
		}catch(NoSuchElementException e) {
			reportStep("Fail", "The Element "+ele+" is not clicked"); 
		}finally {
			takeSnap();			
		}
	}

	@Override
	public boolean verifyTitle(String expectedTitle) {
		WebElement actTitle;
		boolean flag=false;
		try {
			actTitle = driver.findElementByTagName("title");
			if(actTitle.getText().equals(expectedTitle)) {
				reportStep("Pass", "Title "+expectedTitle+" is present");
				flag= true;
			}else {
				flag= false;
			}
		}catch (NoSuchElementException e) {
			reportStep("Fail", "The Element Is Not Located");
		}finally {
			takeSnap();	
		}
		return flag;
	}

	@Override
	public void verifyExactText(WebElement ele, String expectedText) {
		
	}

	@Override
	public void verifyPartialText(WebElement ele, String expectedText) {
		// TODO Auto-generated method stub

	}

	@Override
	public void verifyExactAttribute(WebElement ele, String attribute, String value) {
		// TODO Auto-generated method stub

	}

	@Override
	public void verifyPartialAttribute(WebElement ele, String attribute, String value) {
		// TODO Auto-generated method stub

	}

	@Override
	public void verifySelected(WebElement ele) {
		boolean isSelected = ele.isSelected();
		if(isSelected) {
			reportStep("Pass", ele+" is selected in the screen");
		}else {
			reportStep("Fail", ele+" is not selected in the screen");
		}
		takeSnap();
	}

	@Override
	public void verifyDisplayed(WebElement ele) {
		boolean isDisplayed = ele.isDisplayed();
		if(isDisplayed) {
			reportStep("Pass", ele+" is displayed in the screen");
		}else {
			reportStep("Fail", ele+" is not displayed in the screen");
		}
		takeSnap();
	}

	@Override
	public void switchToWindow(int index) {
		try {
			Set<String> allWindows = driver.getWindowHandles();
			List<String> listOfWindow = new ArrayList<String>();
			listOfWindow.addAll(allWindows);
			driver.switchTo().window(listOfWindow.get(index));
			reportStep("Pass", "The Window is Switched");
		}catch(WebDriverException e) {
			reportStep("Fail", "Window is not Switched");
		}finally {
			takeSnap();
		}
	}

	@Override
	public void switchToFrame(WebElement ele) {
		try {
		driver.switchTo().frame(ele);
		reportStep("Pass", "Switched to the frame"+ele);
		//System.out.println("Switched to the frame "+ele);
		}catch(WebDriverException e) {
			reportStep("Fail", "Window is not Switched");
		}finally {
			takeSnap();
		}
	}

	@Override
	public void acceptAlert() {
		driver.switchTo().alert().accept();
		takeSnap();
	}

	@Override
	public void dismissAlert() {
		driver.switchTo().alert().dismiss();
		takeSnap();
	}

	@Override
	public String getAlertText() {
		String text = driver.switchTo().alert().getText();
		return text;
	}

	@Override
	public void takeSnap() {
		try {
		File src = driver.getScreenshotAs(OutputType.FILE);
		File dsc = new File("./snaps/img"+i+".png");
			FileUtils.copyFile(src, dsc);
		} catch (IOException e) {
			e.printStackTrace();
		}
		i++;
	}

	@Override
	public void closeBrowser() {
		driver.close();
		reportStep("Pass", "Current browser has been closed");
		//System.out.println("Current browser has been closed");
	}

	@Override
	public void closeAllBrowsers() {
		driver.quit();
		reportStep("Pass","All open browsers are closed");
		//System.out.println();
	}

}
