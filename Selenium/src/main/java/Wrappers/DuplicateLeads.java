package Wrappers;
	import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
	import org.testng.annotations.Test;
	public class DuplicateLeads extends ProjectMethods{
		//@Test(groups= {"regression"},dependsOnGroups= {"sanity"})
		@Test(groups= {"regression"})
		public void duplicateLeads() {
			try {
				WebElement crmsfaLink = locateElement("linktext","CRM/SFA");
				clickWithoutSnap(crmsfaLink);
				click(locateElement("linktext","Leads"));
				//String leadID = getText(locateElement("xpath", "((//table[contains(@class,'row-table')])[1]//a)[1]"));
				click(locateElement("xpath", "((//table[contains(@class,'row-table')])[1]//a)[1]"));
				click(locateElement("linktext","Duplicate Lead"));
				click(locateElement("name", "submitButton"));
			}catch (WebDriverException e) {
				reportStep("Fail", "Duplicate Leads failed");
			}
		}
}
