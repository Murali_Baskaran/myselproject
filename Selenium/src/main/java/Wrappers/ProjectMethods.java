package Wrappers;

import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
public class ProjectMethods extends SeMethods {
	@Parameters({"url","username","password"})
	@BeforeMethod(groups="any")
	public void login(String url,String username, String password) {
		try {
			beforeMethod();
			startApp("chrome",url );
			WebElement eleUserName = locateElement("id", "username");
			type(eleUserName,username );
			WebElement elePassword = locateElement("password");
			type(elePassword, password);
			WebElement eleLogin = locateElement("class","decorativeSubmit");
			click(eleLogin);
			reportStep("Pass", "Logged into the application is successful");
		}catch (WebDriverException e) {
			reportStep("Fail", "Application login failed");
		}
	}
	@AfterMethod(groups="any")
	public void closeApp() {
		closeBrowser();
		}
}
