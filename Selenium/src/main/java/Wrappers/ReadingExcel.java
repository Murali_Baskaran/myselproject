package Wrappers;

import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
//import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
//import org.testng.annotations.Test;

public class ReadingExcel {
	//@Test
	public static Object[][] readingXL() throws IOException {
		XSSFWorkbook excel = new XSSFWorkbook("./data/createLeads.xlsx");
		XSSFSheet sheet = excel.getSheetAt(0);
		int rows = sheet.getLastRowNum();
		int cells = sheet.getRow(0).getLastCellNum();
		Object[][] data = new Object[rows][cells];
		for(int i=1;i<=rows;i++) {
			XSSFRow row = sheet.getRow(i);
			for(int j=0;j<cells;j++) {
				XSSFCell cell = row.getCell(j);
				try{
					String value = sheet.getRow(i).getCell(j).getStringCellValue();
					
					data[i-1][j]=value;
					//System.out.println(value);
				}catch (NullPointerException e) {
					data[i-1][j]="";
					System.out.println("");
				}
				
			}
		}
		excel.close();
		return data;
	}
}
