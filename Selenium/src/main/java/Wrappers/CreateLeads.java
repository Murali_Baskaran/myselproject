package Wrappers;
import java.io.IOException;

import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
/*import org.openqa.selenium.WebElement;*/
import org.testng.annotations.Test;

public class CreateLeads extends ProjectMethods{
	@BeforeTest
	public void testCaseDetails() {
		testCaseName = "Create Leads";
		testDesc = "Creating Leads";
		category = "Sanity";
		author = "Kamal";
	}
	//@Test(timeOut=15000)
	@Test(groups= {"smoke"},dataProvider="test")
	public void createLeads(String cName, String fName, String lName, String form) {
		try {
			WebElement crmsfaLink = locateElement("linktext","CRM/SFA");
			clickWithoutSnap(crmsfaLink);
			click(locateElement("linktext","Leads"));
			click(locateElement("linktext","Create Lead"));
			type(locateElement("createLeadForm_companyName"),cName);
			//click(locateElement("xpath", "//a[contains(@href,'parent')]"));
			type(locateElement("createLeadForm_firstName"),fName);
			type(locateElement("createLeadForm_lastName"),lName);
			selectDropDownUsingText(locateElement("createLeadForm_dataSourceId"), form);
			//selectDropDownUsingIndex(locateElement("createLeadForm_marketingCampaignId"), 1);
			click(locateElement("xpath", "//input[@value='Create Lead']"));
		}catch (WebDriverException e) {
			reportStep("Fail", "Create Leads failed");
		}
	}				
	@DataProvider(name="test",indices= {0})
	public Object[][] fetchData() throws IOException{
		Object[][]data = ReadingExcel.readingXL();
	/*Object data[][] = new Object[2][4];
	data[0][0] = "Wipro";
	data[0][1] = "Kumar";
	data[0][2] = "Kannan";
	data[0][3] = "Employee";*/
	/*
	data[1][0] = "IBM";
	data[1][1] = "mahesh";
	data[1][2] = "Kannan";
	data[1][3] = "Employee";*/
	return data;
}
}